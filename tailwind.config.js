module.exports = {
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
    "./plugins/**/*.{js,ts}",
    // "./nuxt.config.{js,ts}",
  ],
  theme: {
    fontFamily: {
      sans: ["Lato", "sans-serif"],
      primario: ["Lato", "sans-serif"],
      secundario: ["Montagu Slab", "serif"],
      tercertipo: ["Grape Nuts", "cursive"],
    },
    extend: {
      color: {
        primario: "000000",
        back: {
          claro: "#FAFAFA",
          oscuro: "#333333",
        },
        front: {
          claro: "#FFFFFF",
          oscuro: "#6A6F74",
        },
        accent: "#1c863e",
        base: "#FFFFFF",
        exito: "#1c863e",
        error: "#871E26",
        info: "#333333",
        aviso: "#871E26",

        blanco: "#FFFFFF",
      },
      minWidth: {
        '80': '80%',
      },
    },
  },
  plugins: [],
}
